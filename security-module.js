/**
 * Sete headers converidos a base64.
 * @param {String} key Clave de la cabecera.
 * @param {String} string Los datos que serán convertidos a base64
 * @param {String} basic Tipo de string enviado.
 */
export const setHeader = (key, string, basic) => {
  if(basic) return { [key]: 'Basic ' + window.btoa(string) }
  return { [key]: window.btoa(string) }
}

/**
 * Almacena el token en el session storage.
 * @param {String} token Token de autenticaicón
 */
export const setSession = (token) => {
  console.log(token)
  sessionStorage.setItem('access-token', token);
}

/**
 * Remueve el token del session storage.
 */
export const removeSession = () => {
  sessionStorage.removeItem('access-token');
}

/**
 * Obtiene el token del session storage.
 */
export const getToken = () => {
  return  sessionStorage.getItem('access-token');
}